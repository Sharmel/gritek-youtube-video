<?php
/**
 * Created by PhpStorm.
 * User: Samsideen
 * Date: 27/05/2018
 * Time: 08:34
 */
return [
    'redis' =>[
        'client' => 'predis',
        'cluster' => 'false',
        'default' => [
            'host' => env('REDIS_HOST','localhost'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT',6379),
            'database' => env('REDIS_DATABASE', 0),
        ],
    ]
];