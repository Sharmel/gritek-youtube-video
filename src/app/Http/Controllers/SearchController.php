<?php
/**
 * Created by PhpStorm.
 * User: capify
 * Date: 01/05/2018
 * Time: 13:10
 */

namespace App\Http\Controllers;

use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SearchController extends Controller
{
    const APPLICATION_NAME = "YouTube Video List";
    const APPLICATION_KEY = "AIzaSyB9J1zXhRfd6RMHoT6WMSWTDn2zolhxP9k";
    protected $arr_base_param = array('location' => '9.081999,8.675277', 'locationRadius' => '1mi',
        'type' => 'video','maxResults' => 20, 'order' => 'viewCount','regionCode' => 'NG');
    protected $str_part = "snippet";
    const DEFAULT_CACHE_TIME = 240; //minutes


    public function setUp()
    {
        $google_client = new \Google_Client();
        $google_client->setApplicationName(self::APPLICATION_NAME);
        $google_client->setDeveloperKey(self::APPLICATION_KEY);

        $service = new \Google_Service_YouTube($google_client);

        return $service;
    }

    public function index()
    {

        try {

           $result = Cache::remember('videos', self::DEFAULT_CACHE_TIME, function () {

               $part = $this->str_part;
               $arr_params = $this->arr_base_param;
               $responses = array();
        $response = $this->setUp()->search->listSearch(
            $part,
            $arr_params
        );

        $responses['videos'] = $response;

        foreach ($response->items as $item) {

            $str_part = 'snippet,statistics,contentDetails';
            $channelId = $item->snippet->channelId;
            $arr_params = array('id' => $channelId, 'maxResults' => 20);
            $channelDetails = $this->setUp()->channels->listChannels(
                $str_part,
                $arr_params
            );

            $arr_country_to_exclude = [null, 'ZA', 'BE'];

            foreach ($channelDetails as $items) {
                $str_country = $items->snippet->country;
                if (!in_array($str_country, $arr_country_to_exclude)) {
                    $responses[] = $channelDetails;
                }
            }


        }

        return $responses;

            });
            return response()->json(compact('result'));



        } catch (ConnectException $e){
            throw $e;
        } catch (ServerException $e){

        } catch (Exception $e){

        }
    }

    public function get(Request $request){

        try {

            $video = Cache::remember('video', self::DEFAULT_CACHE_TIME, function () use ($request) {

                $str_query = $request->route()[2]['query'];

                $part = $this->str_part;
                $arr_params = $this->arr_base_param;
                $arr_params['q'] = $str_query;

                $response = $this->setUp()->search->listSearch(
                $part,
                $arr_params
            );
                return $response;

            });

                return response()->json(compact('video'));


        } catch (ConnectException $e){
            throw $e;
        } catch (ServerException $e){

        } catch (Exception $e){

        }

    }

    /***
     * Show result based on region code
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function show(Request $request){

        try {

            $region_result = Cache::remember('region', self::DEFAULT_CACHE_TIME, function () use ($request) {

                $str_query = $request->route()[2]['query'];

                $part = $this->str_part;
                $arr_params = $this->arr_base_param;
                $arr_params['regionCode'] = $str_query;

                $response = $this->setUp()->search->listSearch(
                $part,
                $arr_params
            );
                return $response;

            });

                return response()->json(compact('region_result'));


        } catch (ConnectException $e){
            throw $e;
        } catch (ServerException $e){

        } catch (Exception $e){

        }

    }

    /***
     * Show result based
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function channelDetails(Request $request){

        try {

            $channelDetails = Cache::remember('channels', self::DEFAULT_CACHE_TIME, function () use ($request) {

            $str_query = $request->route()[2]['query'];

            $str_part = 'snippet,statistics,contentDetails';
            $arr_params = array('id' => $str_query, 'maxResults' => 20);

            $response = $this->setUp()->channels->listChannels(
                $str_part,
                $arr_params
            );

                return $response;

            });

                return response()->json(compact('channelDetails'));


        } catch (ConnectException $e){
            throw $e;
        } catch (ServerException $e){

        } catch (Exception $e){

        }

    }

    /***
     * Show result based on region code
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function channelPlaylist(Request $request)
    {

        try {

            $videoPlaylist = Cache::remember('playlist', self::DEFAULT_CACHE_TIME, function () use ($request) {

                $str_query = $request->route()[2]['query'];

                $str_part = 'snippet,contentDetails';
                $arr_params = array('playlistId' => $str_query, 'maxResults' => 20);

            $response = $this->setUp()->playlistItems->listPlaylistItems(
                $str_part,
                $arr_params
            );

            $responses = array();

            $responses['playlist'] = $response;

            foreach ($response->items as $item) {

                $str_part = 'snippet,contentDetails,statistics';
                $videoId = $item->contentDetails->videoId;

                $arr_params = array('id' => $videoId, 'maxResults' => 20);

                $videoDetails = $this->setUp()->videos->listVideos(
                    $str_part,
                    $arr_params
                );

                $responses[]= $videoDetails;

            }
                return $responses;

            });

            return response()->json(compact('videoPlaylist'));


        } catch (ConnectException $e){
            throw $e;
        } catch (ServerException $e){

        } catch (Exception $e){

        }

    }

    public function videoCategories()
    {
        $part = $this->str_part;
        $arr_params = array('hl' => 'en', 'regionCode' => 'NG');

        try {

            $response = $this->setUp()->videoCategories->listVideoCategories(
                $part,
                $arr_params
            );

            return response()->json(compact('response'));

        } catch (ConnectException $e){
            throw $e;
        } catch (ServerException $e){

        } catch (Exception $e){

        }
    }

}