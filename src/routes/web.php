<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key', function() {
    return str_random(32);
});

$router->group(['prefix' => 'api/v1/youtube'], function ($router) {

    $router->get('/videos', 'SearchController@index');
    $router->get('/videos/categories', 'SearchController@videoCategories');
    $router->get('/video/{query}', 'SearchController@get');
    $router->get('/videos/{query}', 'SearchController@show');
    $router->get('/videos/channels/{query}', 'SearchController@channelDetails');
    $router->get('/videos/channel/playlist/{query}', 'SearchController@channelPlaylist');

});
