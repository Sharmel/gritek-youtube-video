**THE DOCKER CONTAINERS ARE ONLY FOR DEVELOPMENT**
 
Follow the instructions below to get the project up and running. 

Install Composer using Homebrew: 

``` 
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" 
brew tap homebrew/php 
brew install composer
```

Clone this repository 

```git clone https://Sharmel@bitbucket.org/Sharmel/gritek-youtube-video.git``` 

Change directory to the src folder and install project dependencies:

``` cd src ```

Rename ``` .env.example ``` to ``` .env ```

``` composer install ```

Project structure should look like below: 

``` 
YoutubeAPI
├── php
│   └── Dockerfile
├── src
│   └── *
├── README.md
├── site.conf
├── docker-compose.yml
└── mysql
    └── my.cnf
```

Build/run the docker image in the background:

``` docker-compose up -d ``` 


The command above will create images and containers because they've not been created previously, else you can add ``` --build ``` option.

SSH into the running container with:

``` docker exec -it containerID /bin/bash ``` OR ``` docker run -it containerName```

Then run the below command to append to the host file:

``` sudo sh -c 'echo "127.0.0.1   youtube.api" >> /etc/hosts' ``` 

Open your web browser/Postman at ``` youtube.api:9090/api/v1/youtube/videos ``` OR run ``` curl youtube.api:9090/api/v1/youtube/videos ```

The following endpoints are available at the moment:
 
Default location is user location

 ``` 
 /videos
 ```

For searching term from title and description
 
 ``` 
 /video/{query} 
 ```
 
For specifying your preferred location (NG,GB,US, etc). List of country code to use: https://www.iso.org/obp/ui/#search
 
 ``` 
 /videos/{query} 
 ```
 
For fetching details about channel. Get channelID from /videos
 
 ``` 
 /videos/channels/{channelID} 
 ```
 
For fetching channels video playlist. uploadsID is the "uploads" value from the contentDetails section of /videos/channels/{channelID}
  
  ``` 
  /videos/channel/playlist/{uploadsID} 
  ```
  
Load a video by appending videoId from this endpoint /videos/channels/{uploadsID}. 
  
```
http://www.youtube.com/watch?v={video_id_here}

```
